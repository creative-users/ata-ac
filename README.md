** Demo ATA-AC**

This demo shows the related accessibility data available from accessibility.cloud.  Click Lookup Coordinates to geolocate the address (or postal code if address is missing). Then click on Accessibility Cloud to display data with the region boundard centered on the Geo-coordinates.  Data from AirTable that is missing is displayed as 'undefined'.


To run locally:

> npm install

> npm start