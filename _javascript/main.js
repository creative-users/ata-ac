document.addEventListener('DOMContentLoaded', () => {
  console.log("DOM loaded");

  $( "#apikeyButton" ).click(function() {
    loadVenues($('#apikey').val());
  });

  var Airtable = require('airtable');

  var loadVenues = function (apikey) {

    if (apikey == "") {
      alert("Please enter your AirTable API Key");
      return;
    }
    try {
      var base = new Airtable({ apiKey: apikey }).base('appSiDC5N19zTXekl');


      $('#venues').empty();
      var venues =  base('Venues')
      venues.select({
        sort: [{ field: 'Name', direction: 'asc' }]
      }).eachPage(function page(records, fetchNextPage) {
        venueView(records);
        fetchNextPage();
      }, function done(error) {
        console.log(error);
      });
    }
    catch(err) {
      alert("Not able to contact AirTable. Please check your API Key. " + err);
    }

  };


  function lookupCoordinates (record) {
    const baseUrl = 'https://geocoder.ca/?auth=611442295365996364348x6178&geoit=xml&json=1&'
    if (record.get('Address') != null) {
      var params = `${record.get('Address')} ${record.get('City')} ${record.get('Province')}`
      var fetchPromise = fetch(baseUrl + 'locate=' +  encodeURI(params));
    } else if (record.get('Postal Code') != null) {
      var fetchPromise = fetch(baseUrl + 'postal=' + record.get('Postal Code').trim().replace(" ",""));
    }
     var elemId = "div[data-record-id=" + record.getId() + "]";

    fetchPromise
      .then(response => {return response.json();})
      .then(geocoder => {
        console.log(geocoder);
        if (geocoder.longt != null) {
          $(elemId).append($('<div class="box gps">').text(geocoder.latt + ", " + geocoder.longt + ", within 100 meters, " +  geocoder.standard.city));
        } else {
          $(elemId).append($('<div class="box">').text("Could not geo-locate. API limit may be reached."));
        }

      })
  }


  function lookupVenue (record) {
    var elemId = "div[data-record-id=" + record.getId() + "]";
    var coords = $(elemId + " .gps").text();
      console.log(coords);
    var data = {
      appToken: "d58cb193e1fcc73c35705d3fd07b2941",
      latitude: parseFloat(coords.split(",")[0]),
      longitude: parseFloat(coords.split(",")[1]),
      accuracy: 100
    };
    url = buildURL("https://accessibility-cloud.freetls.fastly.net/place-infos", data)

    fetch(url, {headers:{ 'Accept': 'application/json'}})
      .then(
        response => {
          console.log("response",response);
          return response.json();
        },
        error => {console.log(error)}
      )
      .then(
        myJson => {
          console.log(elemId,myJson);
          featureView(myJson, elemId);
        },
        error => {console.log(error)}
      )

  };


  function venueView(records) {
    records.forEach(function (record) {
      if (record.get('Postal Code') != null || record.get('Address') != null) {
        var $venueInfo = $('<div class="box">');
        $venueInfo.append($('<div class="is-size-4">').text(record.get('Name')));
        if (record.get('isPartOf Venue Name') != null) {
          $venueInfo.append($('<div class="tag is-gray">').text(record.get('isPartOf Venue Name')));
          $venueInfo.append($('<br>'));
        }
        if (record.get('Venue Features Lookup') != null) {
          $venueInfo.append($('<div class="tag is-small is-black">').text(record.get('Venue Features Lookup')));
        }
        $venueInfo.append($('<div class="is-size-6">')
          .text(`${record.get('Address')}, ${record.get('City')}, ${record.get('Province')} - ${record.get('Postal Code')}`));
        if (record.get('Latitude') != null) {
          $venueInfo.append($('<div class="tag gps">')
            .text(`${record.get('Latitude')}, ${record.get('Longitude')}`));
        } else {
          $venueInfo.append($('<br>'))
          $venueInfo.append($('<button class="button is-small is-primary">').text('Lookup Coordinates').click(function () {
              lookupCoordinates(record);
            }));
        }

      $venueInfo.append($('<button class="button is-small">').text('Accessibility Cloud').click(function () {
          lookupVenue(record);
        }));
      //  $venueInfo.append($('<div  class="container">').append(x).append(y));
        $venueInfo.attr('data-record-id', record.getId());

        $('#venues').append($venueInfo);
      }
    });

  }

  function featureView(myJson,elemId) {
    var $aInfo = $('<div class="box">');
    $aInfo.append($('<ol>'));
    console.log(myJson)
    if (myJson.featureCount == 0) { $aInfo.append($('<p>').text("No data for these coordinates.")) }
    myJson.features.forEach(function (feature) {
      var lineText =  feature.properties.name + " (" + feature.properties.category + ") - " + JSON.stringify(feature.properties.accessibility);
      if (feature.properties.infoPageUrl != null) {
        $aInfo.append($('<li>').text(lineText)).append($('<a>').text("infoPageUrl").attr('href',feature.properties.infoPageUrl));
      } else {
        $aInfo.append($('<li>').text(lineText));
      }
    });

    $(elemId).append($aInfo);

  }


});





function buildURL (base, data) {
  var url = new URL(base);
  Object.keys(data).forEach(key => url.searchParams.append(key, data[key]));
  return url.href
}